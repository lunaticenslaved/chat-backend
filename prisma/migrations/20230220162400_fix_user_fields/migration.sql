/*
  Warnings:

  - You are about to drop the column `lastSeed` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `passwrod` on the `User` table. All the data in the column will be lost.
  - Added the required column `lastSeen` to the `User` table without a default value. This is not possible if the table is not empty.
  - Added the required column `password` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "lastSeed",
DROP COLUMN "passwrod",
ADD COLUMN     "lastSeen" TIMESTAMP(3) NOT NULL,
ADD COLUMN     "password" TEXT NOT NULL,
ALTER COLUMN "confirmed" SET DEFAULT false,
ALTER COLUMN "avatar" DROP NOT NULL;
