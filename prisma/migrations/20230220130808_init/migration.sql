-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "email" TEXT NOT NULL,
    "passwrod" TEXT NOT NULL,
    "fullname" TEXT NOT NULL,
    "confirmed" BOOLEAN NOT NULL,
    "confirmedHash" TEXT NOT NULL,
    "avatar" TEXT NOT NULL,
    "lastSeed" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");
