/*
  Warnings:

  - You are about to drop the column `confirmationHash` on the `User` table. All the data in the column will be lost.
  - You are about to drop the column `confirmed` on the `User` table. All the data in the column will be lost.
  - Added the required column `activationLink` to the `User` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "User" DROP COLUMN "confirmationHash",
DROP COLUMN "confirmed",
ADD COLUMN     "activationLink" TEXT NOT NULL,
ADD COLUMN     "isActivated" BOOLEAN NOT NULL DEFAULT false;
