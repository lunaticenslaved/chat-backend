declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DATABASE_URL: string;
      REFRESH_TOKEN_MAX_AGE_SECONDS: string;
      JWT_ACCESS_SECRET: string;
      JWT_REFRESH_SECRET: string;
      REFRESH_TOKEN_MAX_AGE_SECONDS: string;
      ACCESS_TOKEN_MAX_AGE_SECONDS: string;
    }
  }
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {};
