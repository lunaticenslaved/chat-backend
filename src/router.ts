import { Router as createRouter } from "express";

import { db as jsonDb } from "./db";
import { controllers } from "@/controllers";
import { authMiddleware } from "@/middlewares";

export const router = createRouter();

router.get("/my-dialogs", authMiddleware, (req, res) => {
  return res.json(jsonDb["my-dialogs"]);
});
router.get("/messages", authMiddleware, (req, res) => {
  return res.json(jsonDb["messages"]);
});
router.get("/users", authMiddleware, (req, res) => {
  return res.json(jsonDb["users"]);
});

router.post(
  "/register",
  ...controllers.user.registerValidation,
  controllers.user.register
);
router.post(
  "/login",
  ...controllers.user.loginValidation,
  controllers.user.login
);
router.post(
  "/logout",
  ...controllers.user.loginValidation,
  controllers.user.login
);
router.post(
  "/activate/:link",
  ...controllers.user.activateValidation,
  controllers.user.activate
);
router.post(
  "/refresh",
  ...controllers.user.refreshTokenValidation,
  controllers.user.refreshToken
);
router.post(
  "/repeat-confirm-email",
  authMiddleware,
  ...controllers.user.repeatConfirmEmailValidation,
  controllers.user.repeatConfirmEmail
);
