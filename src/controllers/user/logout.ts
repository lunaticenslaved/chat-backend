import { NextFunction, Request, Response } from "express";

import { services } from "@/services";

export const logout = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { refreshToken } = req.cookies;
    await services.user.logout(refreshToken);
    res.clearCookie("refreshToken");
    return res.status(200).json();
  } catch (error) {
    next(error);
  }
};

export const logoutValidation = [];
