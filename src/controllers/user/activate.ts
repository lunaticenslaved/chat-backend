import { NextFunction, Request, Response } from "express";

import { services } from "@/services";

export const activate = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const activationLink = req.params.link as string;
    await services.user.activateAccount({ activationLink });
    return res.status(200).json();
  } catch (error) {
    next(error);
  }
};

export const activateValidation = [];
