import { NextFunction, Request, Response } from "express";

import { services } from "@/services";

import { setRefreshTokenCookie } from "../_lib";

export const refreshToken = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { refreshToken } = req.cookies;

    const userData = await services.user.refreshToken(refreshToken);
    setRefreshTokenCookie(res, userData.refreshToken);

    return res.json({
      accessToken: userData.accessToken,
      user: userData.user,
    });
  } catch (error) {
    next(error);
  }
};

export const refreshTokenValidation = [];
