import { NextFunction, Request, Response } from "express";
import { body, validationResult } from "express-validator";

import { ApiError } from "@/errors";
import { services } from "@/services";

import { setRefreshTokenCookie } from "../_lib";

export const register = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      throw ApiError.BadRequest("Ошибка при валидации", errors.array());
    }

    const { email, password, name } = req.body;
    const userData = await services.user.register({ email, password, name });

    setRefreshTokenCookie(res, userData.refreshToken);

    const userDataWithoutRefreshToken = { ...userData, refreshToken: null };
    return res.json(userDataWithoutRefreshToken);
  } catch (error) {
    next(error);
  }
};

export const registerValidation = [
  body("name").isLength({ min: 1 }),
  body("email").isEmail(),
  body("password").isLength({ min: 6 }),
];
