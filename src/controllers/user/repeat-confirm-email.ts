import { NextFunction, Request, Response } from "express";

import { services } from "@/services";

export const repeatConfirmEmail = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    await services.user.repeatConfirmEmail(req.userData.id);
    return res.status(200).json();
  } catch (error) {
    next(error);
  }
};

export const repeatConfirmEmailValidation = [];
