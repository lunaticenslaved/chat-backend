import { NextFunction, Request, Response } from "express";

import { services } from "@/services";

import { setRefreshTokenCookie } from "../_lib";

export const login = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const { email, password } = req.body;
    const userData = await services.user.login({ email, password });
    setRefreshTokenCookie(res, userData.refreshToken);
    const userDataWithoutRefreshToken = {
      accessToken: userData.accessToken,
      user: userData.user,
    };
    return res.json(userDataWithoutRefreshToken);
  } catch (error) {
    next(error);
  }
};

export const loginValidation = [];
