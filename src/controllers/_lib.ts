import { Response } from "express";

export const getRefreshTokenMilliseconds = () => {
  return parseFloat(process.env.REFRESH_TOKEN_MAX_AGE_SECONDS as string) * 1000;
};

export const setRefreshTokenCookie = (res: Response, refreshToken: string) => {
  // with https use secure: true
  res.cookie("refreshToken", refreshToken, {
    maxAge: getRefreshTokenMilliseconds(),
    httpOnly: true,
  });
};
