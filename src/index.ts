import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";

import { router } from "@/router";
import { errorMiddleware } from "@/middlewares";
import { UserDTO } from "@/dto";

// FIXME: убери отсюда расширение. node почему-то не видит его в отдельном файле
declare global {
  namespace Express {
    interface Request {
      userData: UserDTO;
    }
  }
}

const app = express();

app.use(
  cors({
    credentials: true,
    origin: "http://localhost:3000",
  })
);
app.use(cookieParser());
app.use(express.json());
app.use(errorMiddleware);

app.use("/", router);

app.listen(5000, () => {
  console.log("Server started at 5000");
});
