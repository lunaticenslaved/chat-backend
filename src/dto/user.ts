import { User } from "@prisma/client";

export class UserDTO {
  id: number;
  email: string;
  name: string;
  isActivated: boolean;

  constructor(user: User) {
    this.id = user.id;
    this.name = user.name;
    this.email = user.email;
    this.isActivated = user.isActivated;
  }
}
