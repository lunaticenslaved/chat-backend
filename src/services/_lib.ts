import jwt from "jsonwebtoken";

import { UserDTO } from "@/dto";

export const generateTokens = (userData: UserDTO) => {
  const fn = (v: string | undefined) => parseInt(v as string);

  const accessToken = jwt.sign(
    { ...userData },
    process.env.JWT_ACCESS_SECRET as string,
    { expiresIn: fn(process.env.ACCESS_TOKEN_MAX_AGE_SECONDS as string) }
  );

  const refreshToken = jwt.sign(
    { ...userData },
    process.env.JWT_REFRESH_SECRET as string,
    { expiresIn: fn(process.env.REFRESH_TOKEN_MAX_AGE_SECONDS as string) }
  );

  return { accessToken, refreshToken };
};
