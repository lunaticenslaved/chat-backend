import { NextFunction, Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";

import { ApiError } from "@/errors";
import { prisma } from "@/prisma";
import { services } from "@/services";

export const repeatConfirmEmail = async (userId: number) => {
  const user = await prisma.user.findUnique({
    where: { id: userId },
  });

  if (!user) {
    throw ApiError.BadRequest("Пользователь не найден!");
  }

  const activationLink = uuidv4();
  await prisma.user.update({
    where: { id: user.id },
    data: { activationLink: { set: activationLink } },
  });

  await services.mail.sendActivationMail({
    to: user.email,
    link: `${process.env.CLIENT_URL}/activated/${activationLink}`,
  });
};
