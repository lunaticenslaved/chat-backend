import { ApiError } from "@/errors";
import { prisma } from "@/prisma";

export const activateAccount = async (payload: { activationLink: string }) => {
  const { activationLink } = payload;
  const user = await prisma.user.findFirst({
    where: { activationLink: { equals: activationLink } },
  });

  if (!user) {
    throw ApiError.BadRequest("Некорректная ссылка активации");
  }

  await prisma.user.update({
    where: { id: user.id },
    data: { isActivated: { set: true } },
  });
};
