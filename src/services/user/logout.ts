import { services } from "@/services";

export const logout = async (payload: { refreshToken: string }) => {
  await services.session.remove({ refreshToken: payload.refreshToken });
};
