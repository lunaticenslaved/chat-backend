import bcrypt from "bcrypt";

import { ApiError } from "@/errors";
import { prisma } from "@/prisma";
import { services } from "@/services";
import { UserDTO } from "@/dto";

import { generateTokens } from "../_lib";

export const login = async (payload: { email: string; password: string }) => {
  const { email, password } = payload;

  const user = await prisma.user.findUnique({ where: { email } });
  if (!user) {
    throw ApiError.BadRequest("Пользователь с таким email не найден ");
  }

  const isPasswordTheSame = await bcrypt.compare(password, user.password);
  if (!isPasswordTheSame) {
    throw ApiError.BadRequest("Неверный пароль");
  }

  const userDTO = new UserDTO(user);
  const tokens = generateTokens(userDTO);
  await services.session.save(user.id, tokens.refreshToken);

  return { ...tokens, user: userDTO };
};
