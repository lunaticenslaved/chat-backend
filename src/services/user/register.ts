import bcrypt from "bcrypt";
import { v4 as uuidv4 } from "uuid";

import { prisma } from "@/prisma";
import { ApiError } from "@/errors";
import { UserDTO } from "@/dto";

import { services } from "..";
import { generateTokens } from "../_lib";

export const register = async (payload: {
  name: string;
  email: string;
  password: string;
}) => {
  const { email, password, name } = payload;
  const candidate = await prisma.user.findUnique({ where: { email } });

  if (candidate) {
    throw ApiError.BadRequest(`Пользователь с адресом ${email} уже существует`);
  }

  const hashPassword = await bcrypt.hash(password, 3);
  const activationLink = uuidv4();
  const user = await prisma.user.create({
    data: {
      lastSeen: new Date().toISOString(),
      password: hashPassword,
      activationLink,
      email,
      name,
    },
  });

  await services.mail.sendActivationMail({
    to: email,
    link: `${process.env.CLIENT_URL}/activate/${activationLink}`,
  });

  const userDTO = new UserDTO(user);
  const tokens = generateTokens(user);

  await services.session.save(user.id, tokens.refreshToken);

  return { ...tokens, user: userDTO };
};
