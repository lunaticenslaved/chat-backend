import { UserDTO } from "@/dto";
import { ApiError } from "@/errors";
import { prisma } from "@/prisma";
import { services } from "@/services";

import { generateTokens } from "../_lib";

export const refreshToken = async (refreshToken: string) => {
  if (!refreshToken) {
    throw ApiError.UnauthorizedError();
  }

  const userData = await services.session.validateRefreshToken(refreshToken);
  const sessionFromDb = await services.session.find(refreshToken);

  if (!userData || !sessionFromDb) {
    throw ApiError.UnauthorizedError();
  }

  const user = await prisma.user.findUnique({ where: { id: userData.id } });

  if (!user) {
    throw ApiError.BadRequest("Пользователь не найден");
  }

  const userDTO = new UserDTO(user);
  const tokens = generateTokens(userDTO);
  await services.session.save(user.id, tokens.refreshToken);

  return { ...tokens, user: userDTO };
};
