import nodemailer, { Transporter } from "nodemailer";
import SMTPTransport from "nodemailer/lib/smtp-transport";

const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: parseInt(process.env.SMTP_PORT as string),
  // not use TLS. In most cases set this value to true if you are connecting to port 465. For port 587 or 25 keep it false
  secure: false,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASSWORD,
  },
});

export const sendActivationMail = (payload: { to: string; link: string }) => {
  const { to, link } = payload;

  return transporter.sendMail({
    from: process.env.SMTP_USER,
    to,
    subject: "Активация аккаунта на " + process.env.CLIENT_URL,
    text: "",
    html: `
        <div>
          <h1>Для активации перейдите по ссылке</h2>
          <a href="${link}">${link}</a>
        </div>
      `,
  });
};
