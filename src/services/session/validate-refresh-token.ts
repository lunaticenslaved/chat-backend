import jwt from "jsonwebtoken";

import { UserDTO } from "@/dto";

export const validateRefreshToken = (refreshToken: string) => {
  try {
    const userData = jwt.verify(
      refreshToken,
      process.env.JWT_REFRESH_SECRET as string
    ) as UserDTO;
    return userData;
  } catch {
    return null;
  }
};
