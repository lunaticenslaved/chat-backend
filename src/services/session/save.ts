import { Session } from "@prisma/client";
import { prisma } from "@/prisma";

export const save = async (userId: number, refreshToken: string) => {
  const tokenData = await prisma.session.findFirst({ where: { userId } });

  // update existing session
  if (tokenData) {
    const data = await prisma.session.update({
      where: { id: tokenData.id },
      data: { refreshToken: { set: refreshToken } },
    });

    return data as Session;
  }

  // or create new one
  const data = await prisma.session.create({
    data: { userId, refreshToken },
  });

  return data as Session;
};
