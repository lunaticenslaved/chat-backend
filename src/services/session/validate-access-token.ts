import jwt from "jsonwebtoken";

import { UserDTO } from "@/dto";

export const validateAccessToken = (accessToken: string) => {
  try {
    const userData = jwt.verify(
      accessToken,
      process.env.JWT_ACCESS_SECRET as string
    ) as UserDTO;
    return userData;
  } catch {
    return null;
  }
};
