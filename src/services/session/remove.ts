import { prisma } from "@/prisma";

export const remove = async (payload: { refreshToken: string }) => {
  const { refreshToken } = payload;

  await prisma.session.deleteMany({
    where: { refreshToken: { equals: refreshToken } },
  });
};
