import { prisma } from "@/prisma";

export const find = (refreshToken: string) =>
  prisma.session.findFirst({
    where: { refreshToken: { equals: refreshToken } },
  });
