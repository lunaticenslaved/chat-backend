import * as session from "./session";
import * as mail from "./mail";
import * as user from "./user";

export const services = {
  session,
  mail,
  user,
};
