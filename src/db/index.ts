import users from "./users.json";
import myDialogs from "./my-dialogs.json";
import messages from "./messages.json";

export const db = {
  users,
  "my-dialogs": myDialogs,
  messages,
};
