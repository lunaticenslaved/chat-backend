import { NextFunction, Request, Response } from "express";

import { ApiError } from "@/errors";

export const errorMiddleware = (
  err: Error,
  _: Request,
  res: Response,
  __: NextFunction
) => {
  console.error(err);

  if (err instanceof ApiError) {
    return res
      .status(err.status)
      .json({ message: err.message, errors: err.errors });
  } else {
    return res.status(500).json({ message: "Непредвиденная ошибка" });
  }
};
