import { NextFunction, Request, Response } from "express";

import { ApiError } from "@/errors";
import { services } from "@/services";

export const authMiddleware = (
  req: Request,
  _: Response,
  next: NextFunction
) => {
  try {
    const authorizationHeader = req.headers.authorization;

    if (!authorizationHeader) {
      return next(ApiError.UnauthorizedError());
    }

    const accessToken = authorizationHeader.split(" ")[1];

    if (!accessToken) {
      return next(ApiError.UnauthorizedError());
    }

    const userData = services.session.validateAccessToken(accessToken);
    if (!userData) {
      return next(ApiError.UnauthorizedError());
    }

    req.userData = userData;

    next();
  } catch (e) {
    return next(ApiError.UnauthorizedError());
  }
};
